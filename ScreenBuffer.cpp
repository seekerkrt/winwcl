#include "StdAfx.hpp"
#include "ScreenBuffer.hpp"
namespace wcl {
	ScreenBuffer::ScreenBuffer(const HWND hWnd, const COLORREF bgcolor) {
		//クライアント領域サイズを取得
		RECT rect;
		::GetClientRect(hWnd, &rect);
		//親画面サイズと同等の画面バッファ（ビットマップ）を作成と専用HDCの関連付け
		HDC hdc_parent = ::GetDC(hWnd);
		this->hBitmap = ::CreateCompatibleBitmap(hdc_parent, rect.right, rect.bottom);
		this->hdc = ::CreateCompatibleDC(hdc_parent);
		::ReleaseDC(hWnd, hdc_parent);
		::SelectObject(this->hdc, this->hBitmap);
		//ブラシの作成と設定
		this->hBrush = ::CreateSolidBrush(bgcolor);
		SelectBrush(this->hdc, this->hBrush);
		//一応背景クリア
		Rectangle(this->hdc, 0, 0, rect.right, rect.bottom);
	}

	ScreenBuffer::~ScreenBuffer()
	{
		if (hBrush != nullptr) {
			SelectBrush(hdc, nullptr);
			DeleteBrush(hBrush);
		}
		if (hdc != NULL) {
			SelectObject(NULL, hBitmap);
			DeleteDC(hdc);
		}
		if (hBitmap != NULL) {
			DeleteBitmap(hBitmap);
		}
	}
	void ScreenBuffer::Clear() {
		::Rectangle(this->hdc, 0, 0, ::GetDeviceCaps(this->hdc, HORZRES), ::GetDeviceCaps(this->hdc, VERTRES));
	}
	void ScreenBuffer::Clear(COLORREF bgcolor) {
		if (hBrush != nullptr) {
			HBRUSH hbr_old = this->hBrush;
			this->hBrush = ::CreateSolidBrush(bgcolor);
			SelectBrush(this->hdc, this->hBrush);
			DeleteBrush(hbr_old);
		}
		::Rectangle(this->hdc, 0, 0, ::GetDeviceCaps(this->hdc, HORZRES), ::GetDeviceCaps(this->hdc, VERTRES));
	}
};
