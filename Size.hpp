#pragma once
namespace wcl {
	class Size {
	public:
		int x = 0;
		int y = 0;
	public:
		Size() {

		}
		Size(int x, int y) {
			this->Set(x, y);
		}
		//コピーコンストラクタ
		Size(const Size &size) {
			this->x = size.x;
			this->y = size.y;
		}
		virtual ~Size() {

		}

		void Set(int x, int y) {
			this->x = x;
			this->y = y;
		}
	};
};