#pragma once
#include "StdAfx.hpp"
#include <process.h>
//Thread.hpp
//概要：スレッド用基底クラス
//このクラスを継承してスレッドクラスを作る。
//スレッドで処理させたい内容を、仮想関数のrun()をオーバーライドして記述。
//オブジェクト作成後はstart()を呼んで実行する。
//_beginthreadex()を使う




namespace wcl {


	namespace Threading {

		//クラス宣言
		class Thread {
		private:
			HANDLE hThread;	//スレッドハンドル
			//各オブジェクト固有のrun()を実行する関数
			UINT ThreadID;	//スレッドID
			//staticメソッド
			static UINT WINAPI base_start_routine(void* lpThread) {
				Thread *thread = reinterpret_cast<Thread *>(lpThread);
				thread->Run();//各オブジェクトのrun()を実行
				return 0;
			}

		protected:
			void Sleep(DWORD dwMilliseconds);//runメソッド内で呼ぶ。::Sleep(DWORD)と同じ。
			void Notify();	//優先度が同程度以上である他のスレッドに実行権を譲る

		public:
			/*	プライオリティ一覧
				THREAD_PRIORITY_IDLE			-15 低い
				THREAD_PRIORITY_LOWEST			 -2 ↑
				THREAD_PRIORITY_BELOW_NORMAL	 -1
				THREAD_PRIORITY_NORMAL			  0 標準
				THREAD_PRIORITY_ABOVE_NORMAL	  1 ↓
				THREAD_PRIORITY_HIGHEST			  2
				THREAD_PRIORITY_TIME_CRITICAL	 15 高い
			*/
			const static int PRIORITY_IDLE;
			const static int PRIORITY_LOWEST;
			const static int PRIORITY_BACKGROUND;
			const static int PRIORITY_NORMAL;
			const static int PRIORITY_FOREGROUND;
			const static int PRIORITY_HIGHEST;
			const static int PRIORITY_TIME_CRITICAL;

		public:
			Thread();	//コンストラクタ

			virtual ~Thread();	//デストラクタ
		public:
			virtual void Run();//これを継承した場合これをオーバーライドしてスレッドとして処理させる。
		public:
			bool Start();	//スレッドをスタートさせる	
			bool Stop();	//スレッドを一時停止させる
			bool IsAlive();	//スレッドが生きているか？
			void Join();	//このスレッドの終了まで待機する。
			void Terminate();	//スレッドの強制終了（※危険）
			DWORD GetThreadID();//スレッドIDの返値

			int GetPriority() {
				return ::GetThreadPriority(this->hThread);
			}

			BOOL SetPriority(int nPriority) {
				return ::SetThreadPriority(
					this->hThread,    // スレッドハンドル
					nPriority   // 相対優先度
					);
			}
		};//End of class "Thread"

	};//namespace Threading
};