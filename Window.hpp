#pragma once
#include "StdAfx.hpp"
#include "Resource.h"
namespace wcl {
	const int MAX_LOADSTRING = 100;	//ウィンドウクラスネームとアプリケーションタイトルの最大文字数です

	class Window {
	public:
		Window();
		virtual ~Window();
	public:	
		bool Create(HINSTANCE hInstance, TCHAR class_name[MAX_LOADSTRING], TCHAR app_name[MAX_LOADSTRING], int width, int height);
		bool IsCreated();	//ウィンドウが生成（生存）されているか確認
		HWND GetHWND() { return this->hWnd; };
		HINSTANCE GetInstanceHandle();
		bool ShowWindow(int nCmdShow);
		bool UpdateWindow();
		void SetTitle(const TCHAR title[MAX_LOADSTRING]);
		WPARAM GetMessageLoop(UINT wMsgFilterMin, UINT wMsgFilterMax);	//汎用かつ定番のメッセージループ
		WPARAM PeekMessageLoop(UINT wMsgFilterMin, UINT wMsgFilterMax, UINT wRemoveMSG);	//特定用途メッセージループ

		void Invalidate();	//ウィンドウ全体に無効領域を発生させてWM_PAINTを発行する
		void SetWindowPos(int x, int y);
		void SetPrefferedSize(int screen_width, int screen_height);
		void GetWindowRect(RECT* rect);
		void GetClientRect(RECT* rect);
		void AdjustWindowRect(RECT * rect);
		void SetWindowstyle(LONG style);
		LONG GetWindowstyle();

	public:	//カスタマイズするメソッド
		virtual void InnerPeekMessage();//PeekMessageLoopで呼ばれる
		virtual LRESULT LocalWndProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp); //デフォルトではDefWndProcを呼ぶかWM_DESTORYを処理するぐらい

	private:
		static LRESULT CALLBACK BaseWndProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp);	//ラップするためのトリック関数
		void SetPointer(HWND hWnd);	//windowオブジェクト(thisポインタ)とウィンドウハンドルを関連付ける

	protected:
		volatile HWND hWnd = nullptr;
	};
};
