#include "StdAfx.hpp"
#include "Window.hpp"

namespace wcl {
	//コンストラクタ
	Window::Window() {

	}
	//デストラクタ
	Window::~Window() {

	}

	bool Window::Create(HINSTANCE hInstance, TCHAR class_name[MAX_LOADSTRING], TCHAR app_name[MAX_LOADSTRING], int width, int height) {
		WNDCLASSEX wcex;

		wcex.cbSize = sizeof(WNDCLASSEX);	//この構造体自体のサイズなので。
		wcex.cbClsExtra = 0;	//拡張用？とりあえず使わないので0
		wcex.cbWndExtra = 0;	//拡張用？とりあえず使わないので0
		wcex.lpfnWndProc = Window::BaseWndProc;//このウィンドウクラスのプロシージャを指定。このソースコードではこうする
		wcex.hInstance = hInstance;
		wcex.lpszClassName = class_name;
		wcex.lpszMenuName = nullptr;	//メニューの名前。使わないならとりあえずNULLでOK
		wcex.style = CS_VREDRAW | CS_HREDRAW;
		wcex.hCursor = ::LoadCursor(hInstance, IDC_ARROW);		//カーソルアイコン
		wcex.hIcon = ::LoadIcon(hInstance, _T("ICON_NORMAL"));	//プログラムアイコン
		wcex.hIconSm = ::LoadIcon(hInstance, _T("ICON_SMALL"));	//プログラムアイコン（小）::タイトルバーに使われるやつ？
		wcex.hbrBackground = GetStockBrush(BLACK_BRUSH);		//クライアント領域の塗りつぶし色（ブラシ）
		//ATOM登録
		if (!::RegisterClassEx(&wcex)) {
			return false;
		}
		//ウィンドウ生成
		HWND hWnd = ::CreateWindow(
			class_name,
			app_name,
			WS_OVERLAPPEDWINDOW,	//ウィンドウスタイル。とりあえずデフォルトな感じで
			CW_USEDEFAULT, CW_USEDEFAULT,	//初期位置。適当にやってくれる。
			width, height,		//ウィンドウサイズ
			nullptr,	//親ウィンドウのハンドル。特にないんで今回はNULL
			nullptr,	//メニューハンドル。特にないので今回はNULL
			hInstance,
			this	//トリックの肝。lpCreateParamsに設定
			);
		//ウィンドウ生成に失敗？
		if (hWnd == nullptr) {
			return false;
		}
		//成功したのでtrueを返して抜ける
		return true;
	}

	bool Window::IsCreated()
	{
		if (this->hWnd == nullptr) {
			return false;
		}
		else {
			return true;
		}
	}

	bool Window::UpdateWindow() {
		if (this->hWnd == nullptr) {
			return false;
		}
		::UpdateWindow(this->hWnd);
		return true;
	}

	bool Window::ShowWindow(int nCmdShow) {
		if (this->hWnd == nullptr) {
			return false;
		}
		::ShowWindow(this->hWnd, nCmdShow);
		return true;
	}

	void Window::SetTitle(const TCHAR title[wcl::MAX_LOADSTRING]) {
		::SetWindowText(hWnd, title);
	}

	WPARAM Window::GetMessageLoop(UINT wMsgFilterMin, UINT wMsgFilterMax) {
		MSG msg;
		while (::GetMessage(&msg, this->hWnd, wMsgFilterMin, wMsgFilterMax) > 0) {	//「-1」が返ってくるかもしれないのでこうする
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}
		this->hWnd = nullptr;
		return msg.wParam;
	}
	WPARAM Window::PeekMessageLoop(UINT wMsgFilterMin, UINT wMsgFilterMax, UINT wRemoveMSG = PM_REMOVE) {
		MSG msg;
		do {
			//既定の処理はプロシージャなどに任せて……
			if (::PeekMessage(&msg, this->hWnd, wMsgFilterMin, wMsgFilterMax, wRemoveMSG)) {
				::TranslateMessage(&msg);
				::DispatchMessage(&msg);
			}
			//そうでない処理は自前でする
			else {
				//自前の処理であるInnerPeekMessage()を呼び出し
				this->InnerPeekMessage();
			}
		} while (msg.message != WM_QUIT);//イベントドリブン処理ループを抜けるまで実行
		this->hWnd = nullptr;
		return msg.wParam;
	}

	void Window::Invalidate() {
		::InvalidateRect(this->hWnd, nullptr, FALSE);
	}

	void Window::SetWindowPos(int x, int y){
		::SetWindowPos(this->hWnd, nullptr, x, y, 0, 0, SWP_NOSIZE);
	}

	void Window::SetPrefferedSize(int client_width, int client_height){
		RECT c_rect, w_rect;
		::GetWindowRect(this->hWnd, &w_rect);	//現在のウィンドウ全体のサイズ（位置を含む）
		::GetClientRect(this->hWnd, &c_rect);	//現在のクライアント領域のサイズ（位置を含む）
		//サイズ値を修正
		c_rect.right = client_width;
		c_rect.bottom = client_height;
		//修正したクライアント領域サイズでウィンドウ全体のサイズを調整する。
		::AdjustWindowRect(&c_rect, (DWORD)(::GetWindowLongPtr(hWnd, GWL_STYLE)), FALSE);
		::SetWindowPos(hWnd, nullptr, w_rect.left + c_rect.left, w_rect.top + c_rect.top, c_rect.right - c_rect.left, c_rect.bottom - c_rect.top, SWP_NOMOVE);
	}
	HINSTANCE Window::GetInstanceHandle(){
		return (HINSTANCE)::GetWindowLongPtr(this->hWnd, GWLP_HINSTANCE);
	}
	void Window::GetWindowRect(RECT * rect){
		::GetWindowRect(hWnd, rect);
	}

	void Window::GetClientRect(RECT* rect){
		::GetClientRect(hWnd, rect);
	}

	void Window::AdjustWindowRect(RECT* rect) {
		::AdjustWindowRect(rect, (DWORD)(::GetWindowLongPtr(this->hWnd, GWL_STYLE)), FALSE);
	}

	void Window::SetWindowstyle(LONG style) {
		::SetWindowLong(this->hWnd, GWL_STYLE, style);
	}
	LONG Window::GetWindowstyle(){
		return GetWindowStyle(this->hWnd);
	}

	void Window::InnerPeekMessage() {
		//TODO:InnerPeekMessage()
	}

	LRESULT Window::LocalWndProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp) {
		switch (msg) {
		case(WM_DESTROY) :
			::PostQuitMessage(0);
			break;
		default:
			return ::DefWindowProc(hWnd, msg, wp, lp);
		}
		return 0;
	}

	LRESULT CALLBACK Window::BaseWndProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp) {
		//Window::SetPointerでセットしたthisポインタを取得
		Window* window = (Window*)(::GetWindowLongPtr(hWnd, GWLP_USERDATA));
		//取得に失敗してる場合
		if (window == nullptr) {
			//おそらくWM_CREATEの最中なので
			if (msg == WM_CREATE) {
				//CreateWindowのパラメータから取得する
				window = (Window*)(((LPCREATESTRUCT)lp)->lpCreateParams);
			}
			//↑の処理で取得できてるはずなんだけど、一応チェックしてから
			if (window != nullptr) {
				//windowオブジェクトとウィンドウハンドルを関連付ける
				window->SetPointer(hWnd);
			}
		}
		//無事取得できててる
		if (window != nullptr) {
			//そのオブジェクトによるウィンドウプロシージャ実装を使うというか振り分ける
			return window->LocalWndProc(hWnd, msg, wp, lp);
		}
		//よくわからんけど、それでも取得できたりしてない場合（以下のようにするよりも例外作って投げるのがよさそうだがｗ
		else {
			return ::DefWindowProc(hWnd, msg, wp, lp);
		}
	}

	void Window::SetPointer(HWND hWnd) {
		::SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));
		this->hWnd = hWnd;
	}

};
