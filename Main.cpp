#include "StdAfx.hpp"
#include <tchar.h>
#include "Window.hpp"
#include "Thread.hpp"
#include <ctype.h>
#include "Font.hpp"
#include "ScreenBuffer.hpp"


class MyWindow : public wcl::Window {
public:

	//ウィンドウプロシージャ
	virtual LRESULT LocalWndProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp) override{
		static wcl::ScreenBuffer* scr_buff = nullptr;
		static wcl::Font* font = nullptr;
		static const TCHAR* FONT_NAME = wcl::FONTNAME::RICTY;
		static const TCHAR* str = _T("（日本語で表示）こんにちは,Windows-API!! [WrapperClassLibrary]");

		switch (msg) {
		case(WM_CREATE) : {
			if (font == nullptr) {
				font = new wcl::Font(FONT_NAME, 30, true);
			}
			

			break;
		}
		case(WM_PAINT) : {
			//WM_PAINT開始
			PAINTSTRUCT ps;
			HDC hdc = ::BeginPaint(hWnd, &ps);
			if (scr_buff == nullptr) {
				//scr_buff = new wcl::ScreenBuffer(hdc, RGB(0,0,0));
				scr_buff = new wcl::ScreenBuffer(hWnd, RGB(0,0,0));
				SelectFont(scr_buff->GetHDC(), font->GetHFONT());
				SetBkMode(scr_buff->GetHDC(), TRANSPARENT);
				SetTextColor(scr_buff->GetHDC(), RGB(255, 255, 255));
			}
			//裏画面へ描画
			{
				scr_buff->Clear();
				wcl::Size size(scr_buff->GetSize());
				RECT rect = {50, 50, size.x, size.y};
				TCHAR buff[1024];
				//_stprintf_s(buff, _T("offscrbuff->size x=%d,y=%d\n%ls"), size.x, size.y, str);
				swprintf(buff, _T("offscrbuff->size x=%d, y=%d\n%ls"), size.x, size.y, str);
				::DrawText(scr_buff->GetHDC(), buff, lstrlen(buff), &rect, DT_LEFT | DT_WORDBREAK);
			}
			//表画面へ転送
			wcl::Size size = scr_buff->GetSize();
			::BitBlt(hdc, 0, 0, size.x, size.y, scr_buff->GetHDC(), 0, 0, SRCCOPY);
			//WM_PAINT終わり
			::EndPaint(hWnd, &ps);
			break;
		}
		case(WM_CLOSE) :{
			{//後始末
				if (scr_buff != nullptr) {
					delete scr_buff; scr_buff = nullptr;
				}
				if (font != nullptr) {
					delete font; font = nullptr;
				}
			}
			::DestroyWindow(hWnd);
			break;
		}
		case(WM_DESTROY) :
			::PostQuitMessage(0);
			break;
		default:
			return ::DefWindowProc(hWnd, msg, wp, lp);
		}
		return 0;
	}


};

namespace Application{
	int Run(HINSTANCE hInstance, int nCmdShow, TCHAR class_name[wcl::MAX_LOADSTRING], TCHAR app_name[wcl::MAX_LOADSTRING], int width, int height){
        //さーて試してみるか	
		MyWindow wnd;
		//ウィンドウ生成
		if (!wnd.Create(hInstance, class_name, app_name, width, height)) {
			MessageBox(nullptr, _T("Call to Window::Create() is Failed!!"), _T("ErrorInfo"), MB_OK | MB_ICONERROR);
			return 1;
		}
		
		wnd.SetPrefferedSize(width, height);
		//ウィンドウ可視化＆更新
		wnd.ShowWindow(nCmdShow);
		wnd.UpdateWindow();
		
		wnd.SetTitle(_T("WinAPI-Wrapper-Class-Library"));
		//イベントドリブン開始
		return wnd.GetMessageLoop(0, 0);	
	}
};

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdline, int nCmdShow){
	UNREFERENCED_PARAMETER(hPrevInstance);	//おまじない？
	UNREFERENCED_PARAMETER(lpCmdline);      //おまじない？

	TCHAR class_name[wcl::MAX_LOADSTRING] = _T("__WIN32_API_WRAPPPER_CLASS_LIBRARY__");//なんでもいいけど、他のプログラムと被りそうにない文字列
	TCHAR app_name[wcl::MAX_LOADSTRING] = _T("winwcl::Test");	//何でもいい
	auto ret = Application::Run(hInstance, nCmdShow, class_name, app_name, 1280, 768);
	return ret;
}
