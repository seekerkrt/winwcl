//StdAfx.hpp:プリコンパイルヘッダ
#pragma once


// SDKDDKVer.h をインクルードすると、利用できる最も上位の Windows プラットフォームが定義されます。

// 以前の Windows プラットフォーム用にアプリケーションをビルドする場合は、WinSDKVer.h をインクルードし、
// SDKDDKVer.h をインクルードする前に、サポート対象とするプラットフォームを示すように _WIN32_WINNT マクロを設定します。
//#include <WinSDKVer.h>
#define _WIN32_WINNT _WIN32_WINNT_VISTA	//Windows Vista以降をサポート
#define WINVER _WIN32_WINNT
//#include <SDKDDKVer.h>



#define WIN32_LEAN_AND_MEAN             // Windows ヘッダーから使用されていない部分を除外します。
// Windows ヘッダー ファイル:
#include <windows.h>
#include <windowsx.h>

// C ランタイム ヘッダー ファイル
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

// TODO: プログラムに必要な追加ヘッダーをここで参照してください
#include "Resource.h"

#include <string>
namespace std {
	typedef std::basic_string<TCHAR> tstring;
};
#include "SafeDelete.hpp"

