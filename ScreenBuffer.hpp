#pragma once
#include "StdAfx.hpp"
#include "Size.hpp"
namespace wcl {
	//ビットマップ型GDI画面バッファクラス
	class ScreenBuffer {
	protected:
		HDC hdc = nullptr;
		HBITMAP hBitmap = nullptr;
		HBRUSH hBrush = nullptr;


	public:
		ScreenBuffer(const HWND hWnd_pearent, COLORREF bgcolor = RGB(0,0,0));
		virtual ~ScreenBuffer();
	public:
		HDC GetHDC() { return this->hdc; }
		HBITMAP GetHBITMAP() { return this->hBitmap; }
		
		Size GetSize() {
			BITMAP bmp;
			::GetObject(this->hBitmap, sizeof(BITMAP), &bmp);

			Size size = {bmp.bmWidth, bmp.bmHeight};
//			size.Set(::GetDeviceCaps(this->hdc, HORZRES), ::GetDeviceCaps(this->hdc, VERTRES) );
			return size;
		}

		void Clear();
		void Clear(COLORREF bgcolor);
	};
};
