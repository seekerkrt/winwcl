#include "StdAfx.hpp"
#include "Font.hpp"
/*	参考資料
#define LF_FACESIZE    32
typedef struct tagLOGFONT {
    LONG  lfheight;                  // 文字セルまたは文字の高さ
    LONG  lfWidth;                   // 平均文字幅
    LONG  lfEscapement;              // 文字送りの方向とX軸との角度
    LONG  lfOrientation;             // ベースラインとX軸との角度
    LONG  lfWeight;                  // フォントの太さ
    BYTE  lfItalic;                  // イタリック体指定
    BYTE  lfUnderline;               // 下線付き指定
    BYTE  lfStrikeOut;               // 打ち消し線付き指定
    BYTE  lfCharSet;                 // キャラクタセット
    BYTE  lfOutPrecision;            // 出力精度
    BYTE  lfClipPrecision;           // クリッピングの精度
    BYTE  lfQuality;                 // 出力品質
    BYTE  lfPitchAndFamily;          // ピッチとファミリ
    TCHAR lfFaceName[LF_FACESIZE];   // フォント名
} LOGFONT *PLOGFONT, NEAR *NPLOGFONT, FAR *LPLOGFONT;
*/
namespace wcl{
//=====================================================================================================================================
//	コンストラクタ
//=====================================================================================================================================
//コンストラクタ簡易版
Font::Font(LPCTSTR lpszFontName, int fontSize, bool isAntiAlias){
	this->InitializeMember();	//生成前に一度だけメンバ変数を初期化
	this->hFont = this->Create(lpszFontName, fontSize, false, false, false, false, isAntiAlias);
	if(this->hFont == nullptr){
		::MessageBox(nullptr, _T("論理フォントの作成に失敗しました"), nullptr, MB_OK);
	}
}
//コンストラクタ中間版
Font::Font(LPCTSTR lpszFontName, int fontSize, bool isBold, bool isAntiAlias){
	this->InitializeMember();	//生成前に一度だけメンバ変数を初期化
	this->hFont = this->Create(lpszFontName, fontSize, isBold, false, false, false, isAntiAlias);
	if(this->hFont == nullptr){
		::MessageBox(nullptr, _T("論理フォントの作成に失敗しました"), nullptr, MB_OK);
	}
}
//コンストラクタ詳細版
Font::Font(LPCTSTR lpszFontName, int fontSize, bool isBold, bool isItalic, bool isUnderline, bool isStrikeout, bool isAntiAlias){
	this->InitializeMember();	//生成前に一度だけメンバ変数を初期化
	this->hFont = this->Create(lpszFontName, fontSize, isBold, isItalic, isUnderline, isStrikeout, isAntiAlias);
	if(hFont == nullptr){
		::MessageBox(nullptr, _T("論理フォントの作成に失敗しました"), nullptr, MB_OK);
	}
}

//=====================================================================================================================================
//	コンストラクタに呼ばれる特別なメソッド
//=====================================================================================================================================
void Font::InitializeMember(){
	hFont = nullptr;
	lpLogfont = new LOGFONT;	//LOGFONTを生成
	::ZeroMemory(lpLogfont, sizeof(LOGFONT));	//すべて0で初期化
}

//=====================================================================================================================================
//	デストラクタ
//=====================================================================================================================================
Font::~Font(){
	if(hFont != nullptr){
		::DeleteFont(hFont); hFont = nullptr;	//フォントハンドルを削除
	}
	wcl::SafeDelete(lpLogfont);	//LOGFONT構造体オブジェクトをdelete
}
	
//=====================================================================================================================================
//	protected
//=====================================================================================================================================
//フォント生成メソッド
HFONT Font::Create(LPCTSTR lpszFontName, int fontSize, bool isBold, bool isItalic, bool isUnderline, bool isStrikeout, bool isAntiAlias){
	this->lpLogfont->lfHeight =fontSize;	//フォントの高さ。フォントサイズと同義。
	//lp->lfWidth					//平均文字幅。デフォルトだとheightに合わせてくれる。
	//lpLogfont->lfEscapement		//文字送りの方向とX軸との角度:デフォ=0
	//lpLogfont->lfOrientation		//ベースラインとX軸との角度:デフォ=0
//太字の設定
	if(isBold)	this->lpLogfont->lfWeight	=	FW_BOLD;//Boldオン。 MEDIUMかBOLD,EXTRABOLDくらい？
	else		this->lpLogfont->lfWeight	=	FW_NORMAL;	//通常のフォント
	/*	文字の太さ	　数字	別名
		------------------------
		FW_DONTCARE		0			//システム任せ
		------------------------
		FW_THIN			100
		FW_EXTRALIGHT	200
		FW_ULTRALIGHT	200 
		--------------------------------------- 
		FW_LIGHT		300
		FW_NORMAL		400	FW_REGULAR  //このあたりが標準？
		FW_MEDIUM		500  
		-------------------------------------------
		FW_SEMIBOLD		600	FW_DEMIBOLD  
		FW_BOLD			700  
		FW_EXTRABOLD	800	FW_ULTRABOLD
		FW_HEAVY		900	FW_BLACK
	*/
//斜体(Italic)の設定  
    if(isItalic)	this->lpLogfont->lfItalic = TRUE;			//斜体オン
	else		this->lpLogfont->lfItalic = FALSE;		//斜体オフ
//アンダーラインの設定	
	if(isUnderline)	this->lpLogfont->lfUnderline = TRUE;	//下線をつける
	else			this->lpLogfont->lfUnderline = FALSE;	//つけない

	this->lpLogfont->lfCharSet	= SHIFTJIS_CHARSET;	//キャラクタセット
//取り消し線の設定	
	if(isStrikeout)	this->lpLogfont->lfStrikeOut = TRUE;	//取り消し線オン
	else			this->lpLogfont->lfStrikeOut = FALSE;	//取り消し線オフ
	
	this->lpLogfont->lfOutPrecision =	OUT_TT_PRECIS;	//TrueTypeフォント優先。OUT_DEFAULT_PRECISだとデフォルト動作
	this->lpLogfont->lfClipPrecision	=	CLIP_DEFAULT_PRECIS;	//デフォルト
/*	【アンチエイリアス】
	DEFAULT_QUALITY：システム任せ
	NONANTIALIASED_QUALITY：フォントはアンチエイリアス表示されないようにします。
	ANTIALIASED_QUALITY：	対象　NT 4.0/2000/XP
		フォントがアンチエイリアスをサポートしている場合はアンチエイリアス表示になる
	CLEARTYPE_QUALITY： Windows XP以降
		ClearTypeメソッドを使用してアンチエイリアスでレンダリングされます。*/
	//if(isAntiAlias)	this->lpLogfont->lfQuality	= CLEARTYPE_QUALITY;	//アンチエイリアス
	//else			this->lpLogfont->lfQuality	= DEFAULT_QUALITY;		//システム任せ
	this->lpLogfont->lfQuality	= DEFAULT_QUALITY;		//システム任せ

/*	【ピッチ＆ファミリ】
フォントのピッチとファミリを指定します。
ピッチを指定する値とフォントファミリを指定する値をひとつずつ OR 演算子“|”で組み合わせて指定します。
【ピッチ】
　0 DEFAULT_PITCH	デフォルト 
　1 FIXED_PITCH	固定幅
　2 VARIABLE_PITCH	可変幅 
【ファミリ】
　0	FF_DONTCARE	一般的なファミリが指定される。フォントに関する情報が存在しない場合や、重要ではないときに使う。
 16 FF_ROMAN	Serif(H, I などの上下にあるひげ飾り）プロポーショナルフォントが指定される。
					Times New Roman など。
 32 FF_SWISS	Serif『ではない』プロポーショナルフォントが指定されます。 Arialなど。
 48 FF_MODERN	Serifのないモノスペースフォントが指定されます。 Pica, Elite, Courier New など。
 64 FF_SCRIPT	手書き風のデザインのフォントが指定されます。 Script, Cursive など。
 80 FF_DECORATIVE	装飾付きフォントが指定されます。 Old English など。
*/
	this->lpLogfont->lfPitchAndFamily	= DEFAULT_PITCH | FF_DONTCARE;	//ピッチとfamily種類
	::_tcscpy(this->lpLogfont->lfFaceName, lpszFontName);	//フォント名
	
	//いよいよフォントの生成
	return ::CreateFontIndirect(lpLogfont);
}

//=====================================================================================================================================
//	public:Getter
//=====================================================================================================================================
	//HFONTを取得
	HFONT Font::GetHFONT(){
		return hFont;
	}
	//
	TCHAR* Font::GetName(){
		return lpLogfont->lfFaceName;
	}
	int Font::GetSize(){
		return lpLogfont->lfHeight;
	}
	bool Font::IsBold(){
		if(FW_BOLD == lpLogfont->lfWeight)
			return true;
		return false;
	}
	bool Font::IsAntialias(){
//		if(CLEARTYPE_QUALITY == this->lpLogfont->lfQuality)
//			return true;
		return false;
	}
	bool Font::IsUnderline(){
		if(this->lpLogfont->lfUnderline)
			return true;
		return false;
	}
	bool Font::IsItalic(){
		if(this->lpLogfont->lfItalic)
			return true;
		return false;
	}
	bool Font::IsStrikeout(){
		if(this->lpLogfont->lfStrikeOut)
			return true;
		return false;
	}
	
	//同じ設定で別のフォント情報を生成する
	Font* Font::Clone(){
		return new Font(
				GetName(),
				GetSize(),
				IsBold(),
				IsItalic(),
				IsUnderline(),
				IsStrikeout(),
				IsAntialias()
				);
	}

};//namespace ycl;