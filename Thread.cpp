#include "StdAfx.hpp"
#include "Thread.hpp"
namespace wcl {
	namespace Threading {
		//======================================================================================
		//	スレッド優先度定数
		//======================================================================================
			/*	THREAD_PRIORITY_IDLE			-15 低い
				THREAD_PRIORITY_LOWEST			 -2 ↑
				THREAD_PRIORITY_BELOW_NORMAL	 -1
				THREAD_PRIORITY_NORMAL			  0 標準
				THREAD_PRIORITY_ABOVE_NORMAL	  1 ↓
				THREAD_PRIORITY_HIGHEST			  2
				THREAD_PRIORITY_TIME_CRITICAL	 15 高い */
		const int Thread::PRIORITY_IDLE = THREAD_PRIORITY_IDLE;
		const int Thread::PRIORITY_LOWEST = THREAD_PRIORITY_LOWEST;
		const int Thread::PRIORITY_BACKGROUND = THREAD_PRIORITY_BELOW_NORMAL;
		const int Thread::PRIORITY_NORMAL = THREAD_PRIORITY_NORMAL;
		const int Thread::PRIORITY_FOREGROUND = THREAD_PRIORITY_ABOVE_NORMAL;
		const int Thread::PRIORITY_HIGHEST = THREAD_PRIORITY_HIGHEST;
		const int Thread::PRIORITY_TIME_CRITICAL = THREAD_PRIORITY_TIME_CRITICAL;

		//======================================================================================
		//_beginthreadex()版
		//======================================================================================

		//======================================================================================
		//コンストラクタ：_beginthreadex()版
		//======================================================================================
		Thread::Thread() {
			this->hThread = nullptr;
			this->ThreadID = 0;
			//サスペンド状態のスレッドを作成
			this->hThread = reinterpret_cast<HANDLE>(_beginthreadex(
				nullptr, 0, Thread::base_start_routine, this, CREATE_SUSPENDED, &this->ThreadID
				));
		}
		//======================================================================================
		//デストラクタ	
		//======================================================================================
		Thread::~Thread() {
			if (this->IsAlive()) {	//スレッドが動いてるなら強制的に終了させる
				this->Terminate();
			}
			if (this->hThread != nullptr) {	//スレッドが作成されていたなら
				::CloseHandle(this->hThread);	//スレッドのハンドルをクローズ
			}
		}
		//======================================================================================
		//	Runメソッド	
		//======================================================================================
		void Thread::Run() {

		}
		//======================================================================================
		//	publicメソッド定義
		//======================================================================================
		//スレッドが生きているか？
		bool Thread::IsAlive() {
			if (this->hThread != nullptr) {	//スレッドが作成されているか？
				DWORD dwExitCode;
				if (::GetExitCodeThread(this->hThread, &dwExitCode)) {	//スレッドの状態の取得
					if (dwExitCode == STILL_ACTIVE) {	//まだ動作中？
						return true;	//まだ生きている。
					}
				}
			}
			return false;	//スレッド作成に失敗したか既に終了している。
		}


		//スレッドをスタートさせる
		bool Thread::Start() {
			if (this->IsAlive()) {	//スレッドが存在しているか？
				if (::ResumeThread(this->hThread) != 0xFFFFFFFF) {
					return true;	//スレッドスタート成功
				}
			}
			return false;	//スタート失敗
		}

		//スレッドを一時停止させる
		bool Thread::Stop() {
			if (this->IsAlive()) {
				::SuspendThread(this->hThread);
				return true;	//停止成功
			}
			return false;	//スレッドが既にないか、停止に失敗。
		}

		//一定時間スレッド停止
		void Thread::Sleep(DWORD dwMilliseconds) {
			::Sleep(dwMilliseconds);
		}

		//同じ優先度の他のスレッドに実行を譲る
		void Thread::Notify() {
			::Sleep(0);
		}

		//スレッドの強制終了　※危険
		void Thread::Terminate() {
			if (this->IsAlive()) {
				//_endthreadex？
				::TerminateThread(this->hThread, 1);
			}
		}

		//スレッドが終了するまで待つ
		void Thread::Join() {
			/*	戻り値　WaitForSingleObject()
			(WAIT_OBJECT_0) オブジェクトがシグナル状態になったことを示す。
			(WAIT_ABANDONED) 放棄されたためにミューテックスオブジェクトがシグナル状態になったことを示します。
			(WAIT_TIMEOUT) タイムアウト時間が経過したことを示す。
			(WAIT_FAILED) エラーが発生したことを示す。拡張エラー情報を取得するには、GetLastError()を使う。
			*/
			if (this->IsAlive() && this->Start()) {
				::WaitForSingleObject(this->hThread, INFINITE);
			}
		}

		//スレッドIDの返値	
		DWORD Thread::GetThreadID() {
			return this->ThreadID;
		}

	};//namespace Threading;
};
