TP = x86_64-w64-mingw32-
CXX = $(TP)g++
TARGET = winwcl.exe
CXXFLAGS = -std=c++11 -O2 -Wall -Wextra -Wno-unused-parameter -Wfloat-equal -D_UNICODE -DUNICODE -fno-omit-frame-pointer
LIBS = -lgdi32
SOURCES = $(wildcard *.cpp)
RSOURCES = $(wildcard *.rc)
OBJS	= $(SOURCES:.cpp=.o) 
ROBJS	= $(RSOURCES:.rc=.ro)
LDFLAGS = -static -s -mwindows
INCLUDE = 

RC = $(TP)windres
RM = rm -vf

.SUFFIXES: .rc .ro

all: $(TARGET)


$(TARGET): $(OBJS) $(ROBJS)
	$(CXX) $(CXXFLAGS) -o $@ $^ $(LIBS) $(LDFLAGS)	

.rc.ro:
	$(RC) $< $@

%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -c $<

%.cpp: %.hpp


.PHONY: clean
clean:
	$(RM) $(OBJS) *.ro $(TARGET)
