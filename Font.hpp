#pragma once
#include "StdAfx.hpp"
//HFONTとLOGFONTを管理するクラス
//isAntiAliasにtrueを指定するとClearTypeメソッドを使用してフォントをレンダリングするように指示する
namespace wcl{
	namespace FONTNAME{
		const TCHAR MS_GOTHIC[] = _T("ＭＳ ゴシック");
		const TCHAR MS_MINCHOU[] = _T("ＭＳ 明朝");
		const TCHAR MS_UI_GOTHIC[] = _T("MS UI Gothic");
		const TCHAR MPLUS_2VM_IPAG_CIRCLE[] = _T("M+2VM+IPAG circle");
		const TCHAR IPA_GOTHIC[] = _T("IPAゴシック");
		const TCHAR IPA_MINCHOU[] = _T("IPA明朝");
		const TCHAR IPA_UI_GOTHIC[] = _T("IPA UIゴシック");
		const TCHAR MEIRYO[] = _T("メイリオ");
		const TCHAR MEIRYO_UI[] = _T("Meiryo UI");
		const TCHAR RICTY[] = _T("Ricty");
	};


class Font{
public:

public:
	//コンストラクタ
	Font(LPCTSTR lpszFontName, int fontSize, bool isAntiAlias);
	Font(LPCTSTR lpszFontName, int fontSize, bool isBold, bool isAntiAlias);
	Font(LPCTSTR lpszFontName, int fontSize, bool isBold, bool isItalic, bool isUnderline, bool isStrikeout, bool isAntiAlias);
	//デストラクタ
	virtual ~Font();


public://Getter
	HFONT GetHFONT();
	TCHAR* GetName();
	int GetSize();
	LPLOGFONT GetLOGFONT(){
		return lpLogfont;
	}
	bool IsBold();
	bool IsAntialias();
	bool IsUnderline();
	bool IsItalic();
	bool IsStrikeout();

	Font* Clone();

protected:
	void InitializeMember();
	HFONT Create(LPCTSTR lpszFontName, int fontSize, bool isBold, bool isItalic, bool isUnderline, bool isStrikeout, bool isAntiAlias);
	
private:
	LPLOGFONT lpLogfont;
	HFONT hFont;
};//End of class "Font"

};//namespace ycl;

/*	参考資料
#define LF_FACESIZE    32
typedef struct tagLOGFONT {
    LONG  lfheight;                  // 文字セルまたは文字の高さ
    LONG  lfWidth;                   // 平均文字幅
    LONG  lfEscapement;              // 文字送りの方向とX軸との角度
    LONG  lfOrientation;             // ベースラインとX軸との角度
    LONG  lfWeight;                  // フォントの太さ
    BYTE  lfItalic;                  // イタリック体指定
    BYTE  lfUnderline;               // 下線付き指定
    BYTE  lfStrikeOut;               // 打ち消し線付き指定
    BYTE  lfCharSet;                 // キャラクタセット
    BYTE  lfOutPrecision;            // 出力精度
    BYTE  lfClipPrecision;           // クリッピングの精度
    BYTE  lfQuality;                 // 出力品質
    BYTE  lfPitchAndFamily;          // ピッチとファミリ
    TCHAR lfFaceName[LF_FACESIZE];   // フォント名
} LOGFONT *PLOGFONT, NEAR *NPLOGFONT, FAR *LPLOGFONT;
*/