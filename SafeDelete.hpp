#pragma once
namespace wcl{


template <typename T>
inline void SafeDelete(T*& p){
	if(p != nullptr) {
		delete (p);
		(p) = nullptr;
	}
}

template <typename T>
inline void SafeDeleteArray(T*& p){
	if(p != nullptr) {
		delete[] (p);
		(p) = nullptr;
	}
}

//End of namespace ycl;
};